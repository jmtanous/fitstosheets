# fitsTosheets

This is a small project to count fits files produced by Voyager (or any set of files under a directory)
and upload the data to a Google Sheet. The whole idea is to keep the image tally of big astroimaging projects.